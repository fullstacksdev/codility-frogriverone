# codility-FrogRiverOne

Find the earliest time when a frog can jump to the other side of a river. This is the fourth lesson in Codility. You need to find the fastest (earliest) time possible for a frog to start jumping towards the other side of the river. You will be given an array that reflecting the position of jumping/landing point for the frog.